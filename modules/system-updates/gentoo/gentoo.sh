#!/bin/sh

source modules/timeout/timeout.sh

gentoo_updates() {
  # https://gitlab.com/ahrs/dotfiles/blob/master/bin/check-gentoo-updates
  UPDATES_CMD="$HOME/.bin/check-gentoo-updates"
  UPDATES_STATUS="$(timeout -t 1 sudo genlop -ci | head -2 | tail -1 | sed -e 's/^\sCurrently merging\s//g' -e 's/\sout\sof/\//g' -e 's/\s//g' | grep -ohE '[0-9]+\/[0-9]+')"

  UPDATES_FULLTEXT="$($UPDATES_CMD 2> /dev/null)"
  
  [ "$UPDATES_STATUS" != "" ] && UPDATES_FULLTEXT="$UPDATES_STATUS"

  printf "%s\\n" "$UPDATES_FULLTEXT"
}

gentoo_updates
