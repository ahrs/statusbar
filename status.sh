#!/bin/sh

LEMONBAR="${LEMONBAR:-0}"
[ "$LEMONBAR" -eq 1 ] && HAS_LEMONBAR="%%{c}" || HAS_LEMONBAR=""

while true
do
  printf "$HAS_LEMONBAR%s %s %s %s %s %s %s %s %s\\n" \
    "$(modules/bitcoin/bitcoin)" \
    "$(modules/system-updates/system-updates.sh)" \
    "$(modules/bluetooth/bluetooth)" \
    "$(modules/nowplaying/nowplaying)" \
    "$(modules/volume/volume)" \
    "$(modules/disk/disk)" \
    "$(modules/load/load)" \
    "$(modules/date/date)" \
    "$(modules/battery/battery)"
  
  sleep 5
done 2> /dev/null
