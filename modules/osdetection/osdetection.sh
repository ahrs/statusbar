#!/bin/sh

_OS="$(uname -s)"

case "$_OS" in
  Darwin)
    export OS="macOS"
    ;;
  Linux)
    [ -z "$OS" ] && OS="$(grep -E '^ID=' /etc/os-release | sed -e 's/^.*=//g' -e 's/\"//g')"
    case "$OS" in
      ubuntu|debian)
        export OS="debian"
        ;;
      fedora|centos|rhel)
        export OS="redhat"
        ;;
      gentoo)
        export OS="gentoo"
       ;;
      suse|opensuse)
        export OS="suse"
        ;;
      arch|*)
        [ -z "$OS_LIKE" ] && OS_LIKE="$(grep -E '^ID_LIKE=' /etc/os-release | sed -e 's/^.*=//g' -e 's/\"//g')"
        [ "$OS" = "arch" ] && OS_LIKE="arch"
        case "$OS_LIKE" in
          arch|archlinux)
            export OS="arch"
            ;;
          debian)
            export OS="debian"
            ;;
          *)
            ;;
        esac
        ;;
    esac
    ;;

  CYGWIN*|MINGW32*|MSYS*)
    #God help you...
    # msys2
    if command -v pacman > /dev/null 2> /dev/null
    then
      export OS="arch"
    fi
    ;;

  *)
    #some fancy BSD?
    [ "$_OS" = "FreeBSD" ] && export OS="freebsd"
    [ "$_OS" = "OpenBSD" ] && export OS="openbsd"
    ;;
esac

unset _OS
export OS
