#!/usr/bin/env node

const Promisify = require('util').promisify;
const {execSync} = require('child_process');
const execAsync = Promisify(require('child_process').exec);

const SECOND = 1000;
const MINUTE = SECOND * 60;

const DEFAULT_INTERVAL = SECOND * 5;

const modules = [
  {module: "bitcoin", interval: MINUTE * 5},
  {module: "system-updates"},
  {module: "bluetooth"},
  {module: "nowplaying"},
  {module: "volume", interval: 500},
  {module: "disk"},
  {module: "load"},
  {module: "speedtest", interval: Number((MINUTE*2) + MINUTE / 2), lazy_load: true},
  {module: "date", interval: 500},
  {module: "battery", interval: 500, condition: require('fs').existsSync('/sys/class/power_supply/BAT0')}
];

let state = {};

const updateModule = (e, lazy_load) => {
  const interval = (e.hasOwnProperty('interval')) ? e.interval : DEFAULT_INTERVAL;

  if (lazy_load === true) {
    setTimeout(async () => {
      const {stdout,stderr} = await execAsync(`modules/${e.module}/${e.module}`);
      state[e.module] = stdout + (stdout.length > 0 && (e != modules[modules.length-1]) ? ' ' : '');
    }, 0);
  }

  module._interval = setInterval(async () => {
    const {stdout,stderr} = await execAsync(`modules/${e.module}/${e.module}`);

    state[e.module] = stdout + (stdout.length > 0 && (e != modules[modules.length-1]) ? ' ' : '');
  }, interval);

};

const updateModuleSync = e => {
  state[e.module] = (() => {
    const stdout = execSync(`modules/${e.module}/${e.module}`);

    return (stdout.length > 0) ? (stdout + ' ') : stdout;
  })();
};

const template = modules.map(e => {
  if (e.hasOwnProperty('condition')) {
    const condition = (typeof e.condition === 'Function') ? e.condition() : e.condition;
    if (!condition) {
      return "''";
    }
  }

  if (e.hasOwnProperty('lazy_load') && e.lazy_load === true) {
    state[e.module] = '';
  } else {
    updateModuleSync(e);
  }

  updateModule(e, (e.lazy_load === true) ? true : false);

  return `state['${e.module}']`;
}).join(' + ');

const readStatus = () => {
  const status = eval(template).replace(/\n/g, '').replace(/'/g, `'"'"'`);

  if (process.env.DEBUG || process.env.LEMONBAR) {
    console.log(status);
  }

  if (process.env.DWM) {
    execSync(`xsetroot -name '${status}'`);
  }
};

let evtloop = setInterval(readStatus, 500);

const cleanup = code => {
  clearInterval(evtloop);
  modules.forEach(module => {
    if (module.hasOwnProperty('_interval')) {
      clearInterval(module._interval);
    }
  });
  process.exit(0);
};

process.on('SIGINT', cleanup);
process.on('SIGTERM', cleanup);

// Pause on SIGUSR1
process.on('SIGUSR1', code => {
  clearInterval(evtloop);
  modules.forEach(module => {
    if (module.hasOwnProperty('_interval')) {
      clearInterval(module._interval);
    }
  });

  // This is needed to keep the application running
  // The function is essentially a noop so shouldn't
  // consume much CPU.
  evtloop = setInterval(() => {
    return;
  }, 3600);
});

// Resume on SIGUSR2
process.on('SIGUSR2', code => {
  clearInterval(evtloop);
  modules.forEach(e => {
    if (e.hasOwnProperty('lazy_load') && e.lazy_load === true) {
      state[e.module] = '';
    } else {
      updateModuleSync(e);
    }

    updateModule(e, (e.lazy_load === true) ? true : false);
  });
  evtloop = setInterval(readStatus, 500);
});
